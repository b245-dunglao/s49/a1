// console.log("happy monday.")

// Fetch Keyword; 
	// Syntax: 
		// fetch('url',{options});
		// in options - method, body, and headers

fetch('https://jsonplaceholder.typicode.com/posts')
// .then(response => response.json())
.then(response => {
	// console.log(response.json());
	return response.json();
})
.then(result =>{
	console.log(result);
	showPosts(result);
})

// Show post
	// We are going to create a function that will show the posft from our API

const showPosts = (posts) => {
	let entries = '';

	posts.forEach((post)=>{
		entries += ` 
		<div id = "post-${post.id}">
			<h3 id = "post-title-${post.id}">${post.title}</h3>
			<p id = "post-body-${post.id}">${post.body}</p>
			<p><button onClick="editPost(${post.id})">Edit</button><button onClick="deletePost(${post.id})">Delete</button></p>
		</div>`
	})
	document.querySelector(`#div-post-entries`).innerHTML = entries;
}


//POST data on our API
//we need to target the form for creating/adding post

document.querySelector('#form-add-post').addEventListener("submit",(event)=>{
	// preventDefault() function stops the autoreload of the webpage when submitting or onclick of the button. 
	event.preventDefault();



	//post method
	fetch('https://jsonplaceholder.typicode.com/posts',{
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: {
			'Content-Type':'application/json'
		}
	}).then(response=> response.json())
	.then(result => {
		console.log(result);

		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;

		alert("Post is successfully added!");
	})

})


const editPost = (id) => {
		let title = document.querySelector(`#post-title-${id}`).innerHTML;
		let body = document.querySelector(`#post-body-${id}`).innerHTML;
		
		document.querySelector("#txt-edit-id").value = id;
		document.querySelector("#txt-edit-title").value = title;
		document.querySelector("#txt-edit-body").value = body;

		document.querySelector('#btn-submit-update').removeAttribute('disabled');
	}


	// activity
	// deletePost

const deletePost = (id) =>{
		
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
		method: 'DELETE',
		headers: {
			'Content-Type':'application/json'
		}
	})
		alert("Post is successfully removed!");
		document.getElementById(`post-${id}`).remove();
	}



document.querySelector(`#form-edit-post`).addEventListener("submit",(event)=>{
			event.preventDefault();

			let id = document.querySelector("#txt-edit-id").value;

			fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
				method:'PUT',
				headers: {
							'Content-Type':'application/json'
				},
				body: JSON.stringify({
					title: document.querySelector('#txt-edit-title').value,
					body: document.querySelector('#txt-edit-body').value,
					id: id,
					userId: 1
					})
			})
			.then(response => response.json())
			.then(result => {
				console.log(result);
			})

					alert("Edit is successful!");

					document.querySelector("#txt-edit-title").value = null;
					document.querySelector("#txt-edit-body").value = null;
					document.querySelector("#txt-edit-id").value = null;
					document.querySelector('#btn-submit-update').setAttribute('disabled', true);


	})
